#!/usr/bin/env bash

set -e

build() {
    local srcdir="$1"; shift
    local builddir="$1"; shift

    echo -e "\tCopying files in $srcdir to $builddir"
    cp -v "$srcdir/"* -t "$builddir"
}

test() {
    fail="$1"; shift
    if [ "$fail" -eq "1" ]; then
        echo -e "\t FAIL!"
        exit 1
    elif [ "$fail" -eq "2" ]; then
        cp ./toto /tmp
    else
        echo -e "\t OK!"
    fi
}

clean() {
    echo "Cleaning..."
    echo -e "\tRemoving: $BUILDDIR"
    rm -rfv "$BUILDDIR"
}

main() {
    trap clean EXIT
    # Simulate failure:
    # 0 (or nothing) not failure
    # 1 fail in test
    # 2 fail in script (syntax error, failure in execution)
    local simulate_fail="${1:-0}"

    BUILDDIR="./build"
    local builddir="$BUILDDIR"
    mkdir $builddir

    local srcdir="./src"

    echo "BUILDDIR: $builddir"
    echo "SRCDIR: $srcdir"

    echo "Building..."
    build "$srcdir" "$builddir"

    echo "Testing..."
    test "$simulate_fail"
}

main "${@}"