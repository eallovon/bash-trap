# bash-trap

## Description

Playing with bash trap.
In this case very useful to cleanup thing on exit and on error.

### build.sh

This simulates a simple build script:
- build some stuff from a src dir to build dir
- run some test
- clean the build dir when done

It works well when ... it works well :white_check_mark: :
```bash
./build.sh
```
Output:
```
BUILDDIR: ./build
SRCDIR: ./src
Building...
        Copying files in ./src to ./build
'./src/file1' -> './build/file1'
'./src/file2' -> './build/file2'
Testing...
         OK!
Cleaning...
        Removing: ./build
removed './build/file2'
removed './build/file1'
```

But it does not work when :
- :x: test fails (simulated with arg 1):
  ```bash
  ./build.sh 1
  ```
  Output:
  ```
  BUILDDIR: ./build
  SRCDIR: ./src
  Building...
          Copying files in ./src to ./build
  './src/file1' -> './build/file1'
  './src/file2' -> './build/file2'
  Testing...
           FAIL!
  ```
- :x: or script fails (simulated with arg 2):
  ```bash
  ./build.sh 2
  ```
  Output:
  ```
  BUILDDIR: ./build
  SRCDIR: ./src
  Building...
          Copying files in ./src to ./build
  './src/file1' -> './build/file1'
  './src/file2' -> './build/file2'
  Testing...
  cp: cannot stat './toto': No such file or directory
  ```

In both cases, clean step is not run.

### build-with-trap.sh

This simulates a simple build script:
- build some stuff from a src dir to build dir
- run some test
- clean the build dir **via a trap**


It works well when ... it works well :white_check_mark: :
```bash
./build-with-trap.sh
```
Output:
```
BUILDDIR: ./build
SRCDIR: ./src
Building...
        Copying files in ./src to ./build
'./src/file1' -> './build/file1'
'./src/file2' -> './build/file2'
Testing...
         OK!
Cleaning...
        Removing: ./build
removed './build/file2'
removed './build/file1'
removed directory './build'
```

And it still works when :
- :white_check_mark: test fails (simulated with arg 1):
  ```bash
  ./build.sh 1
  ```
  Output:
  ```
  BUILDDIR: ./build
  SRCDIR: ./src
  Building...
          Copying files in ./src to ./build
  './src/file1' -> './build/file1'
  './src/file2' -> './build/file2'
  Testing...
           FAIL!
  Cleaning...
          Removing: ./build
  removed './build/file2'
  removed './build/file1'
  removed directory './build'
  ```
- :white_check_mark: or script fails (simulated with arg 2):
  ```bash
  ./build.sh 2
  ```
  Output:
  ```
  BUILDDIR: ./build
  SRCDIR: ./src
  Building...
          Copying files in ./src to ./build
  './src/file1' -> './build/file1'
  './src/file2' -> './build/file2'
  Testing...
  cp: cannot stat './toto': No such file or directory
  Cleaning...
          Removing: ./build
  removed './build/file2'
  removed './build/file1'
  removed directory './build'
  ```

In all cases, clean step **is run**.

### What's the difference

This is done by the line `trap clean EXIT` which make run the trap at each exit of the script.
Combined with the `set -e` it ensures it is run each time there is a failure.

Note that I had to use a global variable in the `clean` function as it's bit difficult to deal with variable in function called by trap.

Diff between both script:
- using BUILDDIR as a gloabl var
- adding the trap
- removnig the clean step at the end (it is handled by the trap for both clean exit and error/failure exits)

```diff
--- build.sh    2023-10-19 12:16:13.715874078 +0200
+++ build-with-trap.sh  2023-10-19 12:25:08.968784143 +0200
@@ -23,21 +23,23 @@
 }
 
 clean() {
-    local builddir="$1"; shift
-
-    echo -e "\tRemoving: $builddir"
-    rm -rfv "$builddir"
+    echo "Cleaning..."
+    echo -e "\tRemoving: $BUILDDIR"
+    rm -rfv "$BUILDDIR"
 }
 
 main() {
+    trap clean EXIT
     # Simulate failure:
     # 0 (or nothing) not failure
     # 1 fail in test
     # 2 fail in script (syntax error, failure in execution)
     local simulate_fail="${1:-0}"
 
-    local builddir="./build"
+    BUILDDIR="./build"
+    local builddir="$BUILDDIR"
     mkdir $builddir
+
     local srcdir="./src"
 
     echo "BUILDDIR: $builddir"
@@ -48,9 +50,6 @@
 
     echo "Testing..."
     test "$simulate_fail"
-
-    echo "Cleaning..."
-    clean "$builddir"
 }
 
 main "${@}"
\ Pas de fin de ligne à la fin du fichier
```


## Sources

You can find other info:
- [How to Trap Errors in Bash Scripts on Linux](https://www.howtogeek.com/821320/how-to-trap-errors-in-bash-scripts-on-linux/)
- https://www.ludovicocaldara.net/dba/bash-tips-7-cleanup-on-exit/
- [trap command in man bash](https://manpages.debian.org/bookworm/bash/bash.1.en.html#trap)

## License
For open source projects, say how it is licensed.
